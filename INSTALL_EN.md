# **OpenProject docker-compose**

# **Docker setup and deployment**

### Assuming a fresh Ubuntu 20.04 install (also tested on 22.04)

#### **Install Docker and docker-compose**
1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Create docker group if it doesn't exist
sudo groupadd docker
# Add your user to the docker group
sudo usermod -aG docker $USER
# Run the following command or Logout and login again and run (that doesn't work you may need to reboot your machine first)
newgrp docker

# Note: If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

### **Fetch, compile and deploy**
#### Clone the repo
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/openproject-infra.git ~/openproject
```
#### Enter the project folder
```bash
cd ~/openproject
```
#### Copy .env.example into .env
```bash
cp .env.example .env
```
#### Edit environment variables
```bash
nano .env
```
#### Make sure you are using the latest version of the Docker images:
```bash
docker-compose pull
```
#### To start the app up
```bash
docker-compose up
```
#### To run in background
```bash
docker-compose up -d
```
#### To run in background
```bash
docker-compose logs -f
```

### **To Clean Restart of a Docker Instance** 

#### You can stop and remove all containers by running:
```bash
docker-compose down
```
#### Delete containers
```bash
docker rm -f $(docker ps -a -q)
```
This will not remove your data which is persisted in named volumes, likely called compose_opdata (for attachments) and compose_pgdata (for the database). The exact name depends on the name of the directory where your docker-compose.yml files are stored.
#### Delete volumes
```bash
docker volume rm $(docker volume ls -q)
```
#### UP the containers
```bash
docker-compose up -d
```

### **Instructions after starting the containers** 

After a while, OpenProject should be up and running on http://localhost:8080. The default username and password is login: admin, and password: admin. You need to explicitly disable HTTPS mode on startup as OpenProject assumes it’s running behind HTTPS in production by default. After that feel free to use.

## **Backup and upgrade**
* User guide [English](./control/README.md)