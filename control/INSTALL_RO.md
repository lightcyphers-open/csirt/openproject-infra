# Controlați instalarea OpenProject

## Backup

Opriți instalația curentă:

    docker-compose down

Construiți scripturile de control:

    docker-compose -f docker-compose.yml -f docker-compose.control.yml build

Faceți o copie de rezervă a datelor PostgreSQL existente și a activelor OpenProject:

    docker-compose -f docker-compose.yml -f docker-compose.control.yml run backup

Reporniți instalarea OpenProject

    docker-compose up -d

## Actualizare

Opriți instalarea curentă (folosind motorul postgres învechit):

    docker-compose down

Preluați cele mai recente modificări din acest depozit!

Construiți planul de control:

    docker-compose -f docker-compose.yml -f docker-compose.control.yml build

Faceți o copie de rezervă a datelor existente postgresql și a activelor openproject:

    docker-compose -f docker-compose.yml -f docker-compose.control.yml run backup

Rulați upgrade-ul:

    docker-compose -f docker-compose.yml -f docker-compose.control.yml run upgrade

Relansați instalarea OpenProject, folosind comanda normală Compose:

    docker-compose up -d

Testați că totul funcționează din nou, containerul bazei de date ar trebui să ruleze acum postgres 13.