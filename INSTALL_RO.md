# **OpenProject docker-compose**

# **Configurarea și implementarea Docker**

### Presupunând o instalare proaspătă a Ubuntu 20.04 (testat și pe 22.04)

#### **Instalați Docker și docker-compose**
1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Creați grup docker dacă nu există
sudo groupadd docker
# Adăugați utilizatorul în grupul docker
sudo usermod -aG docker $USER
# Rulați următoarea comandă sau Deconectați-vă și conectați-vă din nou și rulați (aceasta nu funcționează, poate fi necesar să reporniți mai întâi mașina)
newgrp docker

# Notă: Dacă comanda docker-compose eșuează după instalare, verificați calea. De asemenea, puteți crea o legătură simbolică către /usr/bin sau orice alt director din calea dvs.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

### **Preluare, compilare și implementare**
#### Clonati depozitul
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/openproject-infra.git ~/openproject
```
#### Intrati în folderul proiectului
```bash
cd ~/openproject
```
#### Copiați .env.example în .env
```bash
cp .env.example .env
```
#### Editați variabilele de mediu
```bash
nano .env
```
#### Asigurați-vă că utilizați cea mai recentă versiune a imaginilor Docker:
```bash
docker-compose pull
```
#### Pentru a porni aplicația
```bash
docker-compose up
```
#### Pentru a rula în fundal
```bash
docker-compose up -d
```
#### Pentru a vizualiza logurile
```bash
docker-compose logs -f
```

### **Pentru a Curăța si Reporni instanța Docker**

#### Puteți opri toate containerele rulând:
```bash
docker-compose stop
```

#### Puteți opri și elimina toate containerele rulând:
```bash
docker-compose down
```
#### Ștergeți containerele
```bash
docker rm -f $(docker ps -a -q)
```
Acest lucru nu va elimina datele dvs. care sunt persistente în volume numite, probabil compose_opdata (pentru atașamente) și compose_pgdata (pentru baza de date). Numele exact depinde de numele directorului în care sunt stocate fișierele docker-compose.yml.
#### Șterge volumele
```bash
docker volume rm $(docker volume ls -q)
```
#### Ridicati containerele
```bash
docker-compose up -d
```

### **Instrucțiuni după pornirea containerelor** 

După un timp, OpenProject ar trebui să funcționeze pe http://localhost:8080. Numele de utilizator și parola implicite sunt login: admin și parola: admin. Trebuie să dezactivați în mod explicit modul HTTPS la pornire, deoarece OpenProject presupune că rulează în urma HTTPS în producție în mod implicit. După aceea, simțiți-vă liber să utilizați.

## **Backup si actualizare**
* Ghid de utilizare [Romana](./control/README.md)